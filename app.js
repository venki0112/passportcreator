var fs = require('fs');
var express = require('express');
var createTemplate = require("passbook");
var bodyParser = require('body-parser');
var app=express();
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(express.static(__dirname));
var jsonParser = bodyParser.json()
var template = createTemplate("boardingPass", {
  passTypeIdentifier: "pass.com.stayzilla.checkinPass",
  teamIdentifier:     "FLJ57G5W97",
  backgroundColor:   "rgb(255, 0, 128)",
  organizationName: "stayzilla",
  foregroundColor : "rgb(255, 255, 255)",
  appLaunchURL:"stayzilla.sz://",
  associatedStoreIdentifiers:[983242704]
});
template.keys(__dirname+"/keys", "secret");
template.loadImagesFrom(__dirname+"/Images");
app.post('/api/getPass',jsonParser, function(req, resp){
	var data = req.body;
	var jsp = "";
		try{
			jsp = data;
			var pass = template.createPass({
  				serialNumber:  jsp.serialNumber,
  				description:   jsp.description,
  				//locations:jsp.location,
  				//relevantDate:jsp.relevantDate
				});
			 jsp.headerFields.forEach(function(key) {
  					pass.headerFields.add(key);
        		});
			  jsp.primaryFields.forEach(function(key) {
  					pass.primaryFields.add(key);
        		});
			  jsp.secondaryFields.forEach(function(key) {
  					pass.secondaryFields.add(key);
        		});
			  jsp.auxiliaryFields.forEach(function(key) {
  					pass.auxiliaryFields.add(key);
        		});
			  jsp.backFields.forEach(function(key) {
  					pass.backFields.add(key);
        		});
			pass.loadImagesFrom("Images");
			var fileName = jsp.serialNumber+".pkpass"
			var file = fs.createWriteStream(fileName);
			pass.on("error", function(error) {
  			console.error(error);
  			process.exit(1);
			})
			pass.pipe(file);

			pass.render(resp, function(error) {
				 fs.unlink(fileName, function() {
  				});
    		if (error)
     			 console.error(error);
  			});
		}
		catch(e){
			console.log(e)
			resp.send(e);
			return;
		}
});
var host = 'localhost';
var port = 3000;
// Start server

var http = require('http').Server(app);
http.listen(port, function(){
  console.log('App started on port ' + port);
});
